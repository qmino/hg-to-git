#!/bin/bash
mkdir -p /tmp/mercurial-repositories
echo "Pushing repositories"
while read p; do
  cd "/tmp/mercurial-repositories/$p"
  hg push -f --new-branch "ssh://hg@bitbucket.org/qmino/hg-backup-$p"
done < /tmp/repositories.txt