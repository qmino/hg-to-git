#!/bin/bash
mkdir -p /tmp/git-repositories
echo "Pushing repositories"
while read p; do
  cd "/tmp/git-repositories/$p"
  git remote add origin "ssh://git@bitbucket.org/qmino/git-migration-$p.git"
  git push origin --all
  git push origin --tags
done < /tmp/repositories.txt