#!/bin/bash
rm /tmp/authors
rm /tmp/deduplicated_authors
touch /tmp/authors
touch /tmp/deduplicated_authors
cd /tmp/mercurial-repositories
while read p; do
  echo "Reading users of: $p"
  cd "$p"
  hg log | grep user: | sort | uniq | sed 's/user: *//' >> /tmp/authors
  cd /tmp/mercurial-repositories
done < /tmp/repositories.txt
cd ..
echo "Deduplicating"
sort /tmp/authors | uniq > /tmp/deduplicated_authors