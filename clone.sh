#!/bin/bash
mkdir -p /tmp/mercurial-repositories
echo "Cloning repositories"
while read p; do
  hg clone "ssh://hg@bitbucket.org/qmino/$p" "/tmp/mercurial-repositories/$p"
done < /tmp/repositories.txt