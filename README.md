# Mercurial to Git Migration
This repository contains a bunch of scripts and Java code that uses Bitbucket's REST API in order to ease migration.

It's not completely automated by design, the reason for this is mostly because I wanted to verify every step of the way.

**DISCLAIMER: Use at own risk.** Some of this code might have destructive effects on your Bitbucket remote's. Be sure to follow the backup steps first.

## Configuration
The Java code that uses the Bitbucket REST API gets its authentication properties from two system properties:

- bitbucket.user
- bitbucket.password

Please check the code in the BitbucketConfig for more info.

## Migration method & Assumptions

This code isn't very tidy and will require some modification for it to work. If there's some interest in this project, I will tidy it up.

Some TODO's:

- The scripts expect the fast-export tool to be installed at `/tmp/fast-export`
- The bash scripts lack parameters for repository baseUrls. Modify them to your desired workspace location. a search and replace for "qmino" with your desired workspace should do the trick.
- The code assumes a single workspace. Change the WORKSPACE constants in BitbucketClient and BitbucketTest
- There is a hardcoded query parameter 'is_private=true' in the Java code. Please modify it if you want to also include public repositories.

### Migration

The main idea of the migration is to have a safe path to move from hg to git repositories, in their original projects, with their original icons and deploy keys.

**Firstly create backup remotes and push clones of our original Hg repositories to it (these remotes will have the hg-backup prefix in their name).**

Next take care of the Hg to git migration on your local machine. Then we're going to create git remotes (in the same projects as our orignals, but prefixed with `git-migration-`) and push our local migrated git repositories. Then we'll update the icons and deploy keys on the remotes.
 
 After this we'll move the original repositories (not deleting), and remove the git prefixes to our newly migrated git repositories.
 
#### Step-by-step

- Generate a list of all Mercurial repositories in `/tmp/repositories.txt`
    - `BitbucketTest.getRepos` does exactly that.
- Create back-up remotes on Bitbucket
    - `BitbucketTest.createHgBackupRemotes` does exactly that for each.
    - `backup-push.sh` push the original Hg repositories to the backup remotes.
- Use `/tmp/repositories.txt` to locally clone and migrate the repositories
    - `clone.sh` clone all Hg repositories of the list (to `/tmp/mercurial-repositories`).
    - `authors.sh` extract authors to `/tmp/deduplicated_authors`
    - Manually map the authors to correct values (see the [git migration guide](https://git-scm.com/book/en/v2/Git-and-Other-Systems-Migrating-to-Git) for more info)
    - `migrate.sh` migrate the local mercurial repositories to git (to `/tmp/git-repositories`).
    - `delete-closed-branches.sh` **optional step** If you don't want closed branches to be present in your git repositories, use this script to delete them. (requires populated `/tmp/mercurial-repositories` and `/tmp/git-repositories` directories)
- Create git remotes and push local repositories
    - `BitbucketTest.createGitRemotes` creates git remotes for each Hg repo.
    - `push.sh` local git repositories to remotes
- `BitbucketTest.migrateIcons` to migrate original icons to git remotes
- `BitbucketTest.setDeployKeys` set access keys to git remotes
- `BitbucketTest.moveActualRepos` prefix the name of your original Hg repositories with `hg-move`
- `BitbucketTest.moveGitMigrationsToActual` remove the prefix of `git-migration-` from the git repositories.
- **Optional**: Delete the moved/backup repositories. There is ready made code snippet for this, but there is a `deleteRepository` method inside the BitbucketClient class.
