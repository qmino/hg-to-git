#!/bin/bash
while read p; do
  cd "/tmp/mercurial-repositories/$p"
  hg log -r "closed()" -T "{branch}\n" | sed -r 's/ |\?/_/g' | sort | uniq >closed.txt
  cd "/tmp/git-repositories/$p"
  git branch -D $(cat "/tmp/mercurial-repositories/$p/closed.txt")
  git push origin --delete $(cat "/tmp/mercurial-repositories/$p/closed.txt")
done </tmp/repositories.txt
