package com.qmino.bitbucket;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.net.http.HttpRequest.BodyPublishers.ofString;

public class BitbucketClient {

    private static final Logger log = Logger.getLogger(BitbucketClient.class.toString());

    public static final String CREATE_JSON_TEMPLATE = "{" +
            "\"is_private\": \"true\"," +
            "\"scm\":\"%s\"," +
            "\"project\": {\"key\": \"%s\"}" +
            "}";
    public static final String WORKSPACE = "qmino";

    private final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_1_1)
            .followRedirects(HttpClient.Redirect.ALWAYS)
            .build();

    private final ObjectMapper objectMapper = new ObjectMapper();

    public BitbucketClient() {
    }

    public JsonNode getRepositories(String workspace, int page) throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .header("Authorization", BitbucketConfig.basicAuth())
                .uri(URI.create(String.format("https://api.bitbucket.org/2.0/repositories/%s?q=is_private=true&page=%d", workspace, page)))
                .build();

        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        if (send.statusCode() >= 400) {
            log.log(Level.SEVERE, send.body());
        }
        return objectMapper.readTree(send.body());
    }

    public List<BitbucketRepo> getAllRepositories(String workspace) throws IOException, InterruptedException {
        JsonNode qmino = getRepositories(WORKSPACE, 1);

        int pagelen = Integer.parseInt(String.valueOf(qmino.get("pagelen")));
        int size = Integer.parseInt(String.valueOf(qmino.get("size")));

        int pages = size / 10;
        if (size % 10 > 0) {
            pages++;
        }

        List<BitbucketRepo> repos = new ArrayList<>();
        for (int i = 1; i < pages + 1; i++) {
            JsonNode repoPage = getRepositories(WORKSPACE, i);
            repoPage.get("values").forEach(repository -> {
                BitbucketRepo repo = new BitbucketRepo(
                        repository.get("project").get("key").asText(),
                        repository.get("full_name").asText().split("/")[0],
                        repository.get("full_name").asText().split("/")[1],
                        repository.get("scm").asText());
                repos.add(repo);
            });
        }
        return repos;
    }

    public List<BitbucketRepo> getAllRepositoriesWithIcons(String workspace) throws IOException, InterruptedException {
        JsonNode qmino = getRepositories(WORKSPACE, 1);

        int pagelen = Integer.parseInt(String.valueOf(qmino.get("pagelen")));
        int size = Integer.parseInt(String.valueOf(qmino.get("size")));

        int pages = size / 10;
        if (size % 10 > 0) {
            pages++;
        }

        List<BitbucketRepo> repos = new ArrayList<>();
        for (int i = 1; i < pages + 1; i++) {
            JsonNode repoPage = getRepositories(WORKSPACE, i);
            repoPage.get("values").forEach(repository -> {
                BitbucketRepo repo = new BitbucketRepo(
                        repository.get("project").get("key").asText(),
                        repository.get("full_name").asText().split("/")[0],
                        repository.get("full_name").asText().split("/")[1],
                        repository.get("scm").asText());
                try {
                    String uri = repository.get("links").get("avatar").get("href").asText();
                    HttpRequest request = HttpRequest.newBuilder()
                            .GET()
                            .uri(URI.create(uri))
                            .build();
                    HttpResponse<byte[]> send = httpClient.send(request, HttpResponse.BodyHandlers.ofByteArray());
                    repo = repo.setIcon(Base64.getEncoder().encodeToString(send.body()));
                } catch (Exception e) {
                    log.log(Level.SEVERE, e, e::getMessage);
                } finally {
                    repos.add(repo);
                }
            });
        }
        return repos;
    }

    public void createRepository(BitbucketRepo bitbucketRepo) throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .POST(ofString(String.format(CREATE_JSON_TEMPLATE,
                        bitbucketRepo.getScm(),
                        bitbucketRepo.getProjectKey())))
                .header("Content-Type", "application/json")
                .header("Authorization", BitbucketConfig.basicAuth())
                .uri(URI.create(String.format(
                        "https://api.bitbucket.org/2.0/repositories/%s/%s",
                        bitbucketRepo.getWorkspace(),
                        bitbucketRepo.getName()
                )))
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        if (send.statusCode() >= 400) {
            log.log(Level.SEVERE, send.body());
        }
    }

    public void deleteRepository(BitbucketRepo bitbucketRepo) throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .DELETE()
                .header("Authorization", BitbucketConfig.basicAuth())
                .uri(URI.create(String.format(
                        "https://api.bitbucket.org/2.0/repositories/%s/%s",
                        bitbucketRepo.getWorkspace(),
                        bitbucketRepo.getName()
                )))
                .build();
        try {
            HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            if (send.statusCode() >= 400) {
                log.log(Level.SEVERE, send.body());
            }
        } catch (IOException e) {
            if (e.getMessage().contains("unexpected content length header with 204 response")) {
                log.warning("Content length header error op 204 aan het negeren.");
            } else {
                throw e;
            }
        }
    }

    public void updateIcon(BitbucketRepo bitbucketRepo) throws IOException, InterruptedException {
        new BitbucketRepo("ON", WORKSPACE, "git-migration-ontrack", "git");
        HttpRequest request = HttpRequest.newBuilder()
                .POST(ofString(String.format("data:image/png;base64,%s", bitbucketRepo.getBase64Icon())))
                .header("Authorization", BitbucketConfig.basicAuth())
                .uri(URI.create(String.format(
                        "https://bitbucket.org/%s/%s/admin/logo",
                        bitbucketRepo.getWorkspace(),
                        bitbucketRepo.getName()
                )))
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        if (send.statusCode() >= 400) {
            log.log(Level.SEVERE, send.body());
        }
    }

    public void setDeployKey(BitbucketRepo bitbucketRepo, String label, String key) throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .POST(ofString(String.format("{\"label\": \"%s\", \"key\": \"%s\"}", label, key)))
                .header("Content-Type", "application/json")
                .header("Authorization", BitbucketConfig.basicAuth())
                .uri(URI.create(String.format(
                        "https://api.bitbucket.org/2.0/repositories/%s/%s/deploy-keys",
                        bitbucketRepo.getWorkspace(),
                        bitbucketRepo.getName()
                )))
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        if (send.statusCode() >= 400) {
            log.log(Level.SEVERE, send.body());
        }
    }

    public void rename(BitbucketRepo bitbucketRepo, String newName) throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .PUT(ofString(String.format("{\"name\": \"%s\"}", newName)))
                .header("Content-Type", "application/json")
                .header("Authorization", BitbucketConfig.basicAuth())
                .uri(URI.create(String.format(
                        "https://api.bitbucket.org/2.0/repositories/%s/%s",
                        bitbucketRepo.getWorkspace(),
                        bitbucketRepo.getName()
                )))
                .build();
        HttpResponse<String> send = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(send.headers().firstValue("Location"));
        if (send.statusCode() >= 400) {
            log.log(Level.SEVERE, send.body());
        }
    }

}
