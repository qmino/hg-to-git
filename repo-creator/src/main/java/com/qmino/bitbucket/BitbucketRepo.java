package com.qmino.bitbucket;

import java.util.function.Predicate;

public class BitbucketRepo {

    public static final String MERCURIAL = "hg";
    public static final String GIT = "git";
    public static final String GIT_MIGRATION = "git-migration-";
    public static final String HG_BACKUP = "hg-backup-";
    public static final String HG_MOVE = "hg-move-";

    public static final Predicate<BitbucketRepo> IS_ORIGINAL_MERCURIAL = repo -> repo.scm.equals(MERCURIAL) && !repo.getName().startsWith(HG_BACKUP);
    public static final Predicate<BitbucketRepo> IS_MIGRATED = repo -> repo.scm.equals(GIT) && repo.getName().startsWith(GIT_MIGRATION);
    public static final Predicate<BitbucketRepo> IS_BACKUP = repo -> repo.scm.equals(MERCURIAL) && repo.getName().startsWith(HG_BACKUP);
    public static final Predicate<BitbucketRepo> IS_MOVE = repo -> repo.scm.equals(MERCURIAL) && repo.getName().startsWith(HG_MOVE);

    private final String projectKey;
    private final String name;
    private final String workspace;
    private final String scm;
    private final String base64Icon;

    public BitbucketRepo(String projectKey, String name, String workspace, String scm, String base64Icon, String deployKey) {
        this.projectKey = projectKey;
        this.name = name;
        this.workspace = workspace;
        this.scm = scm;
        this.base64Icon = base64Icon;
    }

    public BitbucketRepo(String projectKey, String workspace, String name, String scm) {
        this.projectKey = projectKey;
        this.name = name;
        this.workspace = workspace;
        this.scm = scm;
        this.base64Icon = null;
    }

    public BitbucketRepo(String projectKey, String workspace, String name, String scm, String base64Icon) {
        this.projectKey = projectKey;
        this.name = name;
        this.workspace = workspace;
        this.scm = scm;
        this.base64Icon = base64Icon;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public String getName() {
        return name;
    }

    public String getWorkspace() {
        return workspace;
    }

    public String getScm() {
        return scm;
    }

    public String getBase64Icon() {
        return base64Icon;
    }

    public BitbucketRepo setIcon(String icon) {
        return new BitbucketRepo(
                projectKey,
                workspace,
                name,
                scm,
                icon
        );
    }

    public BitbucketRepo toBackup() {
        String newName = HG_BACKUP + name;
        if (scm.equals("git")) {
            throw new IllegalArgumentException("Is een git repo.");
        }
        if (newName.length() > 255) {
            throw new IllegalArgumentException("Repo naam mag niet langer zijn dan 255 karakters.");
        }
        return new BitbucketRepo(projectKey, workspace, newName, "hg", base64Icon);
    }

    public BitbucketRepo toGit() {
        String newName = GIT_MIGRATION + name;
        if (scm.equals("git")) {
            throw new IllegalArgumentException("Is reeds een git repo.");
        }
        if (newName.length() > 255) {
            throw new IllegalArgumentException("Repo naam mag niet langer zijn dan 255 karakters.");
        }
        return new BitbucketRepo(projectKey, workspace, newName, "git", base64Icon);
    }

    @Override
    public String toString() {
        return "BitbucketRepo{" +
                "projectKey='" + projectKey + '\'' +
                ", name='" + name + '\'' +
                ", workspace='" + workspace + '\'' +
                ", scm='" + scm + '\'' +
                '}';
    }
}
