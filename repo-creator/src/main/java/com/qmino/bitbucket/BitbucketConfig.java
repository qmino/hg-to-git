package com.qmino.bitbucket;

import java.util.Base64;

public class BitbucketConfig {

    private static String BITBUCKET_USER;
    private static String BITBUCKET_PASSWORD;

    static {
        BITBUCKET_USER = System.getProperty("bitbucket.user");
        BITBUCKET_PASSWORD = System.getProperty("bitbucket.password");
    }

    public static String basicAuth() {
        return "Basic " + Base64.getEncoder().encodeToString((BitbucketConfig.BITBUCKET_USER + ":" + BitbucketConfig.BITBUCKET_PASSWORD).getBytes());
    }

}
