package com.qmino.bitbucket;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static com.qmino.bitbucket.BitbucketRepo.*;
import static java.net.http.HttpRequest.BodyPublishers.ofString;

class BitbucketTest {

    public static final String WORKSPACE = "qmino";
    public static final String KEY_LABEL = "DEPLOY KEY LABEL";
    public static final String PUBLIC_KEY = "DEPLOY KEY PUBLIC KEY";
    private final BitbucketClient bitbucketClient = new BitbucketClient();

    @Test
    void getRepos() throws Exception {
        File file = new File("/tmp/repositories.txt");
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        FileWriter fileWriter = new FileWriter(file);
        bitbucketClient.getAllRepositories(WORKSPACE).stream()
                .filter(BitbucketRepo.IS_ORIGINAL_MERCURIAL)
                .forEach(repo -> {
                    try {
                        fileWriter.write(repo.getName() + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        fileWriter.flush();
        fileWriter.close();
    }

    @Test
    void getMigratedRepos() throws Exception {
        bitbucketClient.getAllRepositories(WORKSPACE).stream()
                .filter(BitbucketRepo.IS_MIGRATED)
                .forEach(System.out::println);
    }

    @Test
    void getBackupRepos() throws Exception {
        bitbucketClient.getAllRepositories(WORKSPACE).stream()
                .filter(BitbucketRepo.IS_BACKUP)
                .forEach(System.out::println);
    }

    @Test
    void createAndDestroyRepo() throws Exception {
        BitbucketRepo bitbucketRepo = new BitbucketRepo(
                "MD",
                WORKSPACE,
                "unit-test-bitbucket-api",
                "git");
        bitbucketClient.createRepository(bitbucketRepo);
        bitbucketClient.deleteRepository(bitbucketRepo);
    }

    @Test
    void createGitRemotes() throws Exception {
        for (BitbucketRepo bitbucketRepo : bitbucketClient.getAllRepositories(WORKSPACE)) {
            if (BitbucketRepo.IS_ORIGINAL_MERCURIAL.test(bitbucketRepo)) {
                BitbucketRepo toGit = bitbucketRepo.toGit();
                bitbucketClient.createRepository(toGit);
            }
        }
    }

    @Test
    void createHgBackupRemotes() throws Exception {
        for (BitbucketRepo bitbucketRepo : bitbucketClient.getAllRepositories(WORKSPACE)) {
            if (BitbucketRepo.IS_ORIGINAL_MERCURIAL.test(bitbucketRepo)) {
                BitbucketRepo toBackup = bitbucketRepo.toBackup();
                bitbucketClient.createRepository(toBackup);
            }
        }
    }

    @Test
    void moveBackups() throws Exception {
        for (BitbucketRepo repo : bitbucketClient.getAllRepositories(WORKSPACE)) {
            if (BitbucketRepo.IS_BACKUP.test(repo)) {
                bitbucketClient.rename(repo, String.format("%s%s",
                        HG_MOVE,
                        repo.getName().replace(HG_BACKUP, "")
                ));
            }
        }
    }

    @Test
    void moveActualRepos() throws Exception {
        throw new Exception("THIS MOVES YOUR ORIGINAL REPOS");
        //for (BitbucketRepo repo : bitbucketClient.getAllRepositories(WORKSPACE)) {
        //    if (BitbucketRepo.IS_ORIGINAL_MERCURIAL.test(repo)) {
        //        bitbucketClient.rename(repo, String.format("%s%s",
        //                HG_MOVE,
        //                repo.getName()
        //        ));
        //    }
        //}
    }

    @Test
    void moveGitMigrationsToActual() throws Exception {
        for (BitbucketRepo repo : bitbucketClient.getAllRepositories(WORKSPACE)) {
            if (BitbucketRepo.IS_MIGRATED.test(repo)) {
                bitbucketClient.rename(repo, repo.getName().replace(GIT_MIGRATION,""));
            }
        }
    }

    @Test
    void migrateIcons() throws Exception {
        bitbucketClient.getAllRepositoriesWithIcons(WORKSPACE).stream()
                .filter(BitbucketRepo.IS_ORIGINAL_MERCURIAL)
                .forEach(bitbucketRepo -> {
                    try {
                        bitbucketClient.updateIcon(bitbucketRepo.toGit());
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
    }

    @Test
    void setDeployKeys() throws Exception {
        bitbucketClient.getAllRepositories(WORKSPACE).stream()
                .filter(BitbucketRepo.IS_MIGRATED)
                .forEach(repo -> {
                    try {
                        bitbucketClient.setDeployKey(
                                repo,
                                KEY_LABEL,
                                PUBLIC_KEY
                        );
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
    }
}