#!/bin/bash
mkdir -p /tmp/git-repositories
echo "Cloning repositories"
while read p; do
  git init "/tmp/git-repositories/$p"
  cd "/tmp/git-repositories/$p"
  /tmp/fast-export/hg-fast-export.sh -r "/tmp/mercurial-repositories/$p" -A "/tmp/deduplicated_authors_mapped" --force
done < /tmp/repositories.txt